package br.edu.uniateneu.subjetiva.ap2.modelo;
import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
@Entity
@Table(name="tb_aluno")
public class Aluno {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="cd_aluno")
	private Long id;
	
	@Column(name="ds_nome")
	private String nome;
	
	@Column(name="ds_matricula")
	private String matricula;
	
	@Column(name="ds_cpf")
	private String cpf;
	
	@Column(name="ds_rg")
	private String rg;
	
	@Column(name="ds_sexo")
	private String sexo;
	
	
	
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="tb_curso_aluno",
	joinColumns={@JoinColumn(name="cd_curso")},
	inverseJoinColumns={@JoinColumn(name="cd_aluno")})
	private List<Curso> cursos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}

	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
}