package br.edu.uniateneu.subjetiva.ap2.modelo;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tb_curso")
public class Curso {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_curso")
	private Long id;
	
	@Column(name="ds_nome")
	private String nome;
	
	@Column(name="ds_qtdSemestres")
	private Integer qtdSemestres;
	
	@Column(name="ds_tipo")
	private String tipo;
	
	@ManyToMany(mappedBy = "cursos", cascade = CascadeType.ALL)
	private List<Aluno> alunos;
}

