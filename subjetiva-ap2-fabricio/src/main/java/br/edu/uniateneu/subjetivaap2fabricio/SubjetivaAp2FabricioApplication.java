package br.edu.uniateneu.subjetivaap2fabricio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
@EntityScan("br.edu.uniateneu.subjetiva.ap2.modelo*")
@SpringBootApplication
public class SubjetivaAp2FabricioApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubjetivaAp2FabricioApplication.class, args);
	}

}
